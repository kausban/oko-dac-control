/*To compile: gcc  -I/usr/include/libusb-1.0  -lm -Wall oko.c -o oko.o -lftd2xx -lusb-1.0 */
/* accepts voltage value from user and sets all channels to same level
 */

#include <stdio.h>
#include <stdlib.h>
#include <ftd2xx.h>
#include <stdint.h>
#include <string.h>

#define MAX_AMPLITUDE 4096

typedef uint16_t WORDn;
typedef uint32_t DWORDn;
typedef uint8_t BYTEn;

FT_HANDLE DAC; // DAC descriptor

void MakePacket(WORDn *buf,BYTEn *packet);// Function of data package for DAC formation

int dac_zero()
{
	DWORDn BR=0;
	BYTEn packet[130]; // DAC data package
	memset(packet,0,130); //empty packet to zero						
	FT_Write(DAC,packet,130,&BR); // Transfer data package into the DAC
	return 0;
}

int dac_write(WORDn voltage)
{
	WORDn buf[40]; // Buffer of DAC channels
	BYTEn packet[130]; // DAC data package
	DWORDn BR=0;

	dac_zero();
	
	memset(buf, 0, 40*sizeof(WORDn));
	for(int i=0;i<40;i++)
		buf[i]=voltage;

	MakePacket(buf,packet);

	FT_Write(DAC,packet,130,&BR);

	if(BR == 130)
		return 0;
	else
		return -2;
}
	         
int dac_setup(void)
{
	FT_STATUS fs = FT_Open(0,&DAC); // Open a device with system # 0(1,2,etc)
	
	if(fs==FT_OK) // If the device is opened successfully
		{
			dac_zero();
			return 0;
		}
	else
		return -1;
} // End dac_setup()
//---------------------------------------------------------------------------
static BYTEn DAC_CHANEL_TABLE[40]= // Table of DAC channels
	{
		/*DAC-> 0 1 2 3 4 |
		  ---------------------+ OUTPUT */
		7, 15, 23, 31, 39, //| A
		6, 14, 22, 30, 38, //| B
		5, 13, 21, 29, 37, //| C
		4, 12, 20, 28, 36, //| D
		3, 11, 19, 27, 35, //| E
		2, 10, 18, 26, 34, //| F
		1,  9, 17, 25, 33, //| G
		0,  8, 16, 24, 32  //| H
	};
//---------------------------------------------------------------------------
void MakePacket(WORDn *buf,BYTEn *packet)
/* Form a data packet from the buffer of channels:
   buf - an input array consisting of forty 16-digit words, which code
   voltage levels of the outputs ##1-40
   packet – the resulting 129-byte output array to be transferred into the unit via
   USB bus
*/
{
	BYTEn *p=packet+1;
	for(int i=0,s=0;i<8;i++,s+=5)
		{
			/*			printf("i=%d s=%d \n",i,s);
			 */
			
			// Form address parts of control words for five DAC chips
			*(p++)=0;
			*(p++)=(i&4)?0x1f:0;
			*(p++)=(i&2)?0x1f:0;
			*(p++)=(i&1)?0x1f:0;
			

			/*			for(int z=4;z>0;z--)
				printf("%d",*(p-z));

				printf("\n");*/
			
			// form control codes from the array of voltages according to the table
			for(int j=0,mask=0x800;j<12;j++,mask>>=1)
				*(p++)=
					((buf[DAC_CHANEL_TABLE[s+0]]&mask)?0x01:0) |
					((buf[DAC_CHANEL_TABLE[s+1]]&mask)?0x02:0) |
					((buf[DAC_CHANEL_TABLE[s+2]]&mask)?0x04:0) |
					((buf[DAC_CHANEL_TABLE[s+3]]&mask)?0x08:0) |
					((buf[DAC_CHANEL_TABLE[s+4]]&mask)?0x10:0) ;
		}
	packet[0] = 0xff; // non-zero starting byte
}

int main()
{
	int err_code;
	err_code=dac_setup();
	WORDn voltage=0;
	while(err_code == 0)
		{
			
			err_code=dac_write(voltage);
			char *end;
			char str1[20];
			
			printf("\n Enter voltage value (< 4096) or 9999 to stop. \n");

			if (!fgets(str1, sizeof(str1), stdin))
				break;

			// remove \n
			str1[strlen(str1) - 1] = 0;
			WORDn n = strtol(str1, &end, 10);

			if(n == 9999)
				{				  
					FT_Close(DAC); // Close the device
					err_code=1;					
				}

			voltage=n;
		}

	
	printf("\nError %d\n",err_code);
	return err_code;
}

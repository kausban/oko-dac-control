#! /bin/bash

if [ "$1" = 0 ];then
#	terminate
	cat /tmp/okoRun.pid | xargs  kill -TERM
	
elif [ "$1" = 1 ];then
#	continue
	cat /tmp/okoRun.pid | xargs  kill -USR1

else
	echo "0 terminate | 1 continue"
	
fi

exit 0


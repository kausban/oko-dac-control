/*To compile:  gcc  -I/usr/include/libusb-1.0  -lm -Wall okofileSig.c -o okofileSig.o -lftd2xx -lusb-1.0 */

/*
writes pid number to /tmp/okoRun.pid

accepts USR1 signal to continue read and dac_write loop
accepts TERM signal to die gracefully

see signal.sh to see how to send USR1 and TERM signal with bash

*/

#include <stdio.h>
#include <stdlib.h>
#include <ftd2xx.h> 
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#define MAX_AMPLITUDE 4095

typedef uint16_t WORDn;
typedef uint32_t DWORDn;
typedef uint8_t BYTEn;

FT_HANDLE DAC; // DAC descriptor
WORDn buf[40] = {0}; // Buffer of DAC channels
int TERMFLAG = 0; //FLAG used to gracefully terminate from while loop

void MakePacket(WORDn *buf,BYTEn *packet);// Function of data package for DAC formation
int dac_zero();

int dac_write()
{
	BYTEn packet[130]; // DAC data package
	DWORDn BR=0;

	dac_zero();

	MakePacket(buf,packet);

	FT_Write(DAC,packet,130,&BR);

	if(BR == 130)
		return 0;
	else
		return -2;
}
	         
int dac_setup(void)
{
	FT_STATUS fs = FT_Open(0,&DAC); // Open a device with system # 0(1,2,etc)
	
	if(fs==FT_OK) // If the device is opened successfully
		{
			dac_zero();			
			return 0;
		}
	else
		return -1;
} // End dac_setup()

int dac_zero()
{
	DWORDn BR=0;
	BYTEn packet[130]; // DAC data package
	memset(packet,0,130); //empty packet to zero						
	FT_Write(DAC,packet,130,&BR); // Transfer data package into the DAC
	return 0;
}
//---------------------------------------------------------------------------
static BYTEn DAC_CHANEL_TABLE[40]= // Table of DAC channels
	{
		/*DAC-> 0 1 2 3 4 |
		  ---------------------+ OUTPUT */
		7, 15, 23, 31, 39, //| A
		6, 14, 22, 30, 38, //| B
		5, 13, 21, 29, 37, //| C
		4, 12, 20, 28, 36, //| D
		3, 11, 19, 27, 35, //| E
		2, 10, 18, 26, 34, //| F
		1,  9, 17, 25, 33, //| G
		0,  8, 16, 24, 32  //| H
	};
//---------------------------------------------------------------------------
void MakePacket(WORDn *buf,BYTEn *packet)
/* Form a data packet from the buffer of channels:
   buf - an input array consisting of forty 16-digit words, which code
   voltage levels of the outputs ##1-40
   packet – the resulting 129-byte output array to be transferred into the unit via
   USB bus
*/
{
	BYTEn *p=packet+1;
	for(int i=0,s=0;i<8;i++,s+=5)
		{			
			// Form address parts of control words for five DAC chips
			*(p++)=0;
			*(p++)=(i&4)?0x1f:0;
			*(p++)=(i&2)?0x1f:0;
			*(p++)=(i&1)?0x1f:0;
				
			// form control codes from the array of voltages according to the table
			for(int j=0,mask=0x800;j<12;j++,mask>>=1)
				*(p++)=
					((buf[DAC_CHANEL_TABLE[s+0]]&mask)?0x01:0) |
					((buf[DAC_CHANEL_TABLE[s+1]]&mask)?0x02:0) |
					((buf[DAC_CHANEL_TABLE[s+2]]&mask)?0x04:0) |
					((buf[DAC_CHANEL_TABLE[s+3]]&mask)?0x08:0) |
					((buf[DAC_CHANEL_TABLE[s+4]]&mask)?0x10:0) ;
		}
	packet[0] = 0xff; // non-zero starting byte
}

int read_file() //read file volts to make array of 40 voltages
{
	FILE *txtf;
	txtf = fopen("volts","r");
	if (txtf == NULL)
		return -3;

	char *end;
	char *line=NULL;
	size_t len=0;
	
	fprintf(stderr,"\n");	
	for(int j=0; j<40; j++) {
		getline(&line,&len,txtf);			// Reads jth line
		line[strlen(line) - 1] = 0;
		WORDn n = strtol(line, &end, 10);

		buf[j]=n;		
		fprintf(stderr,"Channel %d = %dV\n", j+1, buf[j]); //output current 
	}

	fclose(txtf);
	return 0;
}

void sig_handler (int signo) //handle signals
{
	if (signo == SIGUSR1)
		fprintf(stderr,"\nreceived SIGUSR1\n\n");
	else if (signo == SIGTERM)
		{
			fprintf(stderr,"\nreceived SIGTERM\n\n");
			FT_Close(DAC);
			TERMFLAG=1;
		}
}

int write_pid() // write current PID to file
{
	FILE *runf;
	runf = fopen("/tmp/okoRun.pid","w");
	if (runf == NULL)
		{
			fprintf(stderr,"Error opening PID file\n");
			return -4;
		}
	fprintf(runf,"%d\n",getpid());
	fprintf(stderr,"PID : %d\n", getpid());

	fclose(runf);
	return 0;
}

int main()
{
	int err_code=0;

	if (signal(SIGUSR1, sig_handler) == SIG_ERR)
		fprintf(stderr,"\ncan't catch SIGUSR1\n");

	if (signal(SIGTERM, sig_handler) == SIG_ERR)
		fprintf(stderr,"\ncan't catch SIGTERM\n");

	err_code+=write_pid();
	err_code+=dac_setup();
	err_code+=read_file();
	
	while(err_code == 0)
		{
			err_code=read_file();
			err_code=dac_write();			
			
			/* Wait for a signal to arrive. */
			pause();
			err_code=TERMFLAG;
		}

	
	fprintf(stderr,"\nError %d\n",err_code);
	return err_code;
}

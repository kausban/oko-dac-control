# OKO dac control

oko.c handles single voltage level for all channels
okofile reads voltages from file "volts" in same folder
okofileSig does same as okofile but waits for USR1 or TERM signal to continue (POSIX signals)

needs ftdi D2XX direct drivers (libftd2xx). Include header file <ftd2xx.h>.

gcc compile commands in comments of source files.
